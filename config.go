package main

import (
	"encoding/json"
	"gitlab.com/gbh007/golog"
	"os"
	"sync"
	"time"
)

type fileInfo struct {
	Code string `json:"code"`
	Path string `json:"path"`
}

type configData struct {
	DataDir       string     `json:"data_dir"`
	Filelist      []fileInfo `json:"filelist"`
	URL           string     `json:"url"`
	Token         string     `json:"token"`
	Send          bool       `json:"send"`
	Port          int        `json:"port"`
	RegistratorID int        `json:"registrator_id"`
	Interval      int        `json:"interval"`
}

// Config структура конфига
type Config struct {
	Data configData
	Log  *golog.Log
}

var _config *Config
var _once sync.Once

func getConfig() *Config {
	_once.Do(func() { _config = new(Config); _config.load() })
	return _config
}

func (cnf *Config) load() {
	w, err := os.OpenFile("log/log.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, os.ModePerm)
	if err == nil {
		cnf.Log = golog.NewWriterLog(w, "", golog.LogLevelDebug)
	} else {
		cnf.Log = golog.New("")
		cnf.Log.Error(err)
	}
	cnfFile, err := os.Open("cnf/cnf.json")
	defer cnfFile.Close()
	if err != nil {
		cnf.Log.Panic(err)
	}
	err = json.NewDecoder(cnfFile).Decode(&cnf.Data)
	if err != nil {
		cnf.Log.Panic(err)
	}
}

func (cnf *Config) save() error {
	err := os.Rename("cnf/cnf.json", "cnf/"+time.Now().Format("2006-01-02 15-04-05")+".json")
	if err != nil {
		cnf.Log.Error(err)
	}
	cnfFile, err := os.Create("cnf/cnf.json")
	defer cnfFile.Close()
	if err != nil {
		cnf.Log.Error(err)
		return err
	}
	err = json.NewEncoder(cnfFile).Encode(&cnf.Data)
	if err != nil {
		cnf.Log.Error(err)
		return err
	}
	return nil
}
