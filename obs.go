package main

import (
	"gitlab.com/gbh007/gofo"
	"path"
	"time"
)

type stabJSON struct {
	ID            int    `json:"id"`
	StationCode   string `json:"station_code"`
	RegistratorID int    `json:"registrator_id"`
	Time          string `json:"time"`
	Delta         int64  `json:"delta"`
	Reason        string `json:"reason"`
	Lost          bool   `json:"lost"`
}

func (stb *stabJSON) load(st *gofo.Message) *stabJSON {
	stb.ID = -1
	stb.StationCode = st.Code
	stb.RegistratorID = getConfig().Data.RegistratorID
	stb.Time = st.LastUpdate.UTC().Format("2006-01-02 15:04:05")
	stb.Delta = int64(st.Delta / time.Second)
	stb.Reason = ""
	stb.Lost = st.Lost
	return stb
}

// ObsController контроллер обсерверов
type ObsController struct {
	obsList  []*gofo.Observer
	messages chan *gofo.Message
	Comm     chan int
	run      bool
}

func newObsCont() *ObsController {
	return &ObsController{
		obsList:  make([]*gofo.Observer, 0),
		messages: make(chan *gofo.Message, 1000),
		Comm:     make(chan int, 100),
	}
}

func (oc *ObsController) start() {
	if oc.run {
		return
	}
	cnf := getConfig()
	checkers := []time.Duration{
		time.Second * time.Duration(cnf.Data.Interval),
	}
	for _, file := range cnf.Data.Filelist {
		obs := gofo.NewObserver(file.Code, path.Join(cnf.Data.DataDir, file.Path), checkers)
		obs.C = oc.messages
		oc.obsList = append(oc.obsList, obs)
	}
	for _, obs := range oc.obsList {
		go obs.Run()
	}
	oc.run = true
}

func (oc *ObsController) stop() {
	if !oc.run {
		return
	}
	for _, obs := range oc.obsList {
		obs.Kill()
	}
	oc.obsList = make([]*gofo.Observer, 0)
	oc.run = false
}

// Run запускает контроллер файлового наблюдателя
func (oc *ObsController) Run() {
	notSend := make([]*stabJSON, 0)
	cnf := getConfig()
	for {
		select {
		case comm := <-oc.Comm:
			switch comm {
			case wStart:
				oc.start()
				cnf.Log.Info("Наблюдатель запущен")
			case wStop:
				oc.stop()
				cnf.Log.Info("Наблюдатель остановлен")
			case wRestart:
				oc.stop()
				cnf.Log.Info("Наблюдатель остановлен")
				oc.start()
				cnf.Log.Info("Наблюдатель запущен")
			case wKill:
				oc.stop()
				cnf.Log.Info("Наблюдатель остановлен")
				return
			}
		case mess := <-oc.messages:
			m := (&stabJSON{}).load(mess)
			cnf.Log.Info(m)
			if !getConfig().Data.Send {
				continue
			}
			if sendMess(m) != nil {
				notSend = append(notSend, m)
			}
			if len(notSend) > 1 {
				tmp := make([]*stabJSON, 0)
				for _, m := range notSend {
					if sendMess(m) != nil {
						tmp = append(tmp, m)
					}
				}
				notSend = tmp
			}
		}
	}
}
