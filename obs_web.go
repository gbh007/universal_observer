package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

func sendMess(mess *stabJSON) error {
	cnf := getConfig()
	buf := &bytes.Buffer{}
	json.NewEncoder(buf).Encode(mess)
	req, err := http.NewRequest(http.MethodPost, cnf.Data.URL, buf)
	if err != nil {
		cnf.Log.Error(err)
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.AddCookie(&http.Cookie{Name: "seismo_journal_token", Value: cnf.Data.Token})
	res, err := (&http.Client{Timeout: time.Second * 10}).Do(req)
	if err != nil {
		cnf.Log.Error(err)
		return err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		cnf.Log.Info("code", res.StatusCode)
		return fmt.Errorf("not send, code: %d", res.StatusCode)
	}
	return nil
}

// GetData возвращает данные о текущих задержках
func (oc *ObsController) GetData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Content-Type", "application/json")
	if r.Method == http.MethodOptions {
		w.Header().Set("Allow", "POST, OPTIONS")
		return
	}
	w.WriteHeader(http.StatusOK)
	type row struct {
		Code  string `json:"code"`
		Delta int64  `json:"delta"`
	}
	res := make([]*row, 0)
	for _, obs := range oc.obsList {
		res = append(res, &row{
			Code:  obs.Code,
			Delta: int64(obs.GetDelta() / time.Second),
		})
	}
	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		getConfig().Log.Error(err)
	}
}
