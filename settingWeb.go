package main

import (
	"encoding/json"
	"io"
	"net/http"
)

func getSettingWeb(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Content-Type", "application/json")
	if r.Method == http.MethodOptions {
		w.Header().Set("Allow", "POST, OPTIONS")
		return
	}
	w.WriteHeader(http.StatusOK)
	cnf := getConfig()
	err := json.NewEncoder(w).Encode(cnf.Data)
	if err != nil {
		cnf.Log.Error(err)
	}
}

func setSettingWeb(w http.ResponseWriter, r *http.Request) {
	cnf := getConfig()
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Content-Type", "application/json")
	if r.Method == http.MethodOptions {
		w.Header().Set("Allow", "POST, OPTIONS")
		return
	}
	tmp := configData{}
	err := json.NewDecoder(r.Body).Decode(&tmp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w, err.Error())
		cnf.Log.Error(err)
		return
	}
	cnf.Data = tmp
	err = cnf.save()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w, err.Error())
		cnf.Log.Error(err)
		return
	}
	cnf.Log.Info("конфигурация обновлена")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(cnf.Data)
	if err != nil {
		cnf.Log.Error(err)
	}
	wCom <- wRestart
}
