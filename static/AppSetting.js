var app = new Vue({
  el: "#app",
  data: {
    setting: {}
  },
  mounted: function() {
    this.load();
  },
  methods: {
    deleteFile(code) {
      this.setting.filelist = this.setting.filelist.filter(e => e.code != code);
    },
    load() {
      let self = this;
      axios
        .post("/api/setting/get")
        .then(function(response) {
          self.setting = response.data;
        })
        .catch(e => alert(e));
    },
    save() {
      let self = this;
      axios
        .post("/api/setting/set", self.setting)
        .then(function(response) {
          self.setting = response.data;
        })
        .catch(e => alert(e));
    }
  }
});
