function getRandomColor() {
  let letters = "0123456789ABCDEF".split("");
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
var app = new Vue({
  el: "#app",
  data: {
    stat: [],
    gdata: {},
    chart: null,
    colors: {},
    on: {},
    interval: 10,
    fullTime: 600,
    round: 0.5
  },
  mounted: function() {
    this.load();
  },
  methods: {
    clear() {
      this.gdata = {};
    },
    style(delta) {
      return (
        "color:" +
        (delta >= 600 ? "white" : delta > 120 ? "blue" : "black") +
        "; background:" +
        (delta >= 600 ? "red" : delta > 120 ? "yellow" : "green")
      );
    },
    updateChart() {
      let datasets = [];
      let ml = 0;
      let gcount = Math.round(this.fullTime / this.interval);
      for (let code in this.gdata) {
        if (this.colors[code] == undefined) {
          this.colors[code] = getRandomColor();
        }
        if (this.on[code] == true) {
          ml = Math.max(ml, this.gdata[code].length);
          datasets.push({
            label: code,
            borderColor: this.colors[code],
            showLine: true,
            fill: false,
            lineTension: this.round,
            data: this.gdata[code].slice(-gcount)
          });
        }
      }
      ml = Math.min(ml, gcount);
      if (this.chart == null) {
        this.chart = Chart.Line(this.$refs.gr, {});
        this.chart.options = {
          animation: { duration: 0 },
          title: { display: false }
        };
      }
      let labels = [];
      for (let i = 0; i < ml; i++) {
        labels.push((ml - i) * this.interval);
      }
      this.chart.data = {
        datasets,
        labels
      };
      this.chart.update();
    },
    load() {
      this.stat = [];
      let self = this;
      axios.post("/api/data").then(function(response) {
        response.data.forEach(e => {
          self.stat.push(e);
          if (self.gdata[e.code] == undefined) {
            self.gdata[e.code] = [];
          }
          self.gdata[e.code].push(e.delta);
          if (self.gdata[e.code].length * self.interval > self.fullTime) {
            self.gdata[e.code].shift();
          }
        });
        self.stat.sort((a, b) =>
          a.code < b.code ? -1 : a.code > b.code ? 1 : 0
        );
        self.$nextTick(self.updateChart);
      });
      setTimeout(this.load, this.interval * 1000);
    }
  }
});
