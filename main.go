package main

import (
	"context"
	"fmt"
	"net/http"
)

const (
	wStop int = iota
	wStart
	wRestart
	wKill
)

var wCom chan int

func main() {
	wCom = make(chan int, 100)
	cnf := getConfig()
	oc := newObsCont()
	go oc.Run()
	http.HandleFunc("/api/setting/get", getSettingWeb)
	http.HandleFunc("/api/setting/set", setSettingWeb)
	http.HandleFunc("/api/data", oc.GetData)
	http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir("static"))))
	http.Handle("/log.txt", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { http.ServeFile(w, r, "log/log.txt") }))
	server := &http.Server{Addr: fmt.Sprintf(":%d", cnf.Data.Port), Handler: nil}
	wCom <- wStart
	for {
		switch <-wCom {
		case wStart:
			oc.Comm <- wStart
			go func() {
				server = &http.Server{Addr: fmt.Sprintf(":%d", cnf.Data.Port), Handler: nil}
				cnf.Log.Info("веб сервер запущен")
				server.ListenAndServe()
				err := server.ListenAndServe()
				if err != nil && err != http.ErrServerClosed {
					cnf.Log.Error(err)
				}
			}()
		case wStop:
			oc.Comm <- wStop
			if err := server.Shutdown(context.Background()); err != nil {
				cnf.Log.Error(err)
			}
		case wRestart:
			oc.Comm <- wRestart
			if err := server.Shutdown(context.Background()); err != nil {
				cnf.Log.Error(err)
			}
			go func() {
				server = &http.Server{Addr: fmt.Sprintf(":%d", cnf.Data.Port), Handler: nil}
				cnf.Log.Info("веб сервер запущен")
				server.ListenAndServe()
				err := server.ListenAndServe()
				if err != nil && err != http.ErrServerClosed {
					cnf.Log.Error(err)
				}
			}()
		}
	}
}
