FROM amd64/alpine
LABEL maintainer="Никониров Григорий mrgbh007@gmail.com"
RUN mkdir /app
RUN mkdir /app/log
COPY main /app/main
COPY cnf /app/cnf
COPY static /app/static
WORKDIR /app
EXPOSE 80
CMD ["/app/main"]

# docker build -t universal_observer .
# docker save universal_observer -o universal_observer.tar